import { ReactNode } from 'react'

import { Header } from '@/components/Header'
import { CartProvider } from '@/contexts/CartContext'

type Props = {
  children: ReactNode
}

export default function StoreLayout({ children }: Props) {
  return (
    <CartProvider>
      <div className="mx-auto grid min-h-screen w-full max-w-[1600px] grid-rows-app gap-5 px-8 py-8">
        <Header />
        {children}
      </div>
    </CartProvider>
  )
}
